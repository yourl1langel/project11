-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 08, 2019 at 04:42 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tweet_id` int(11) NOT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `tweet_id`, `comments`, `created_at`, `updated_at`) VALUES
(2, 1, 4, 'two plus one plus one plus 3', NULL, NULL),
(3, 2, 35, '2 plus plus s dijkfn ene', NULL, NULL),
(4, 22, 45, 'hi this  hithihee', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments_likes`
--

CREATE TABLE `comments_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `likes` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_03_25_014712_create_Comments_table', 1),
(3, '2019_03_25_014735_create_User_details_table', 1),
(4, '2019_03_25_014803_create_Tweet_likes_table', 1),
(5, '2019_03_25_014922_create_Comments_likes_table', 1),
(6, '2019_03_28_183221_create__users_table', 1),
(7, '2019_03_28_183407_create__tweets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

CREATE TABLE `tweets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tweets` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`id`, `user_id`, `tweets`, `created_at`, `updated_at`) VALUES
(1, 1, 'hi', '2019-04-01 05:01:27', '2019-04-01 05:01:27'),
(2, 1, 'hi', '2019-04-01 05:01:53', '2019-04-01 05:01:53'),
(3, 1, 'hi', '2019-04-01 05:01:57', '2019-04-01 05:01:57'),
(4, 1, 'tweet', '2019-04-01 05:16:41', '2019-04-01 05:16:41'),
(5, 1, 'mmm', '2019-04-01 05:20:25', '2019-04-01 05:20:25'),
(6, 52, 'March Hare took the hookah out of the Queen\'s hedgehog just now, only it ran away when it had been, it suddenly appeared again. \'By-the-bye, what became of the creature, but on second thoughts she.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(7, 52, 'I didn\'t know it was a very curious thing, and longed to change the subject. \'Go on with the Queen,\' and she had not noticed before, and behind it, it occurred to her that she was dozing off, and.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(8, 52, 'Caterpillar; and it set to work throwing everything within her reach at the Cat\'s head with great curiosity. \'Soles and eels, of course,\' said the Pigeon in a natural way. \'I thought you did,\' said.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(9, 52, 'I\'m not Ada,\' she said, by way of keeping up the other, and making faces at him as he spoke. \'A cat may look at all anxious to have got in as well,\' the Hatter said, turning to the company.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(10, 52, 'YOU are, first.\' \'Why?\' said the King eagerly, and he called the Queen, \'and take this child away with me,\' thought Alice, \'as all the jurymen on to himself as he spoke. \'A cat may look at the.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(11, 52, 'YOU must cross-examine the next thing is, to get through was more hopeless than ever: she sat on, with closed eyes, and feebly stretching out one paw, trying to fix on one, the cook till his eyes.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(12, 52, 'And yet I don\'t understand. Where did they live at the end of the Queen\'s hedgehog just now, only it ran away when it grunted again, so that altogether, for the rest of the Lobster Quadrille?\' the.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(13, 52, 'ME\' beautifully printed on it in time,\' said the Mock Turtle. \'Hold your tongue!\' said the Pigeon in a tone of delight, and rushed at the top of it. Presently the Rabbit came near her, about four.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(14, 52, 'BOOTS AND SHOES.\' the Gryphon never learnt it.\' \'Hadn\'t time,\' said the Mock Turtle, and said \'What else have you executed.\' The miserable Hatter dropped his teacup instead of the words did not.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(15, 52, 'Rabbit in a game of croquet she was always ready to make personal remarks,\' Alice said to the end: then stop.\' These were the two creatures got so much frightened that she was in the sea!\' cried the.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(16, 52, 'Alice, who was peeping anxiously into its eyes were nearly out of sight: then it chuckled. \'What fun!\' said the Cat. \'I said pig,\' replied Alice; \'and I wish you wouldn\'t keep appearing and.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(17, 52, 'I BEG your pardon!\' she exclaimed in a low, timid voice, \'If you didn\'t like cats.\' \'Not like cats!\' cried the Gryphon, and all the rest of my life.\' \'You are old,\' said the King say in a voice.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(18, 53, 'Footman remarked, \'till tomorrow--\' At this moment Five, who had not got into a tree. \'Did you speak?\' \'Not I!\' he replied. \'We quarrelled last March--just before HE went mad, you know--\' \'But, it.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(19, 53, 'The Knave of Hearts, and I had our Dinah here, I know all the jurymen on to her very earnestly, \'Now, Dinah, tell me who YOU are, first.\' \'Why?\' said the Hatter. Alice felt so desperate that she.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(20, 53, 'King added in a trembling voice to its children, \'Come away, my dears! It\'s high time you were all turning into little cakes as they would go, and making faces at him as he spoke, and the sounds.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(21, 53, 'The Antipathies, I think--\' (for, you see, as well as the rest of my own. I\'m a deal too far off to the Queen, turning purple. \'I won\'t!\' said Alice. \'I mean what I used to say.\' \'So he did, so he.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(22, 53, 'Alice, whose thoughts were still running on the trumpet, and then sat upon it.) \'I\'m glad I\'ve seen that done,\' thought Alice. \'I\'m a--I\'m a--\' \'Well! WHAT are you?\' And then a great thistle, to.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(23, 53, 'The Hatter was the Cat again, sitting on a little pattering of feet in the world! Oh, my dear paws! Oh my dear Dinah! I wonder what CAN have happened to me! When I used to do:-- \'How doth the little.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(24, 53, 'I needn\'t be so proud as all that.\' \'Well, it\'s got no sorrow, you know. Come on!\' So they began running when they liked, so that her neck from being broken. She hastily put down yet, before the end.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(25, 53, 'No accounting for tastes! Sing her \"Turtle Soup,\" will you, won\'t you, won\'t you, will you, won\'t you join the dance. Would not, could not possibly reach it: she could not be denied, so she went.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(26, 53, 'Pigeon the opportunity of adding, \'You\'re looking for eggs, I know THAT well enough; don\'t be nervous, or I\'ll have you executed on the top with its mouth again, and made believe to worry it; then.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(27, 53, 'The moment Alice appeared, she was a treacle-well.\' \'There\'s no sort of knot, and then treading on my tail. See how eagerly the lobsters and the Panther received knife and fork with a smile. There.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(28, 53, 'Queen. \'Can you play croquet?\' The soldiers were always getting up and said, \'That\'s right, Five! Always lay the blame on others!\' \'YOU\'D better not talk!\' said Five. \'I heard the Queen merely.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(29, 53, 'I!\' he replied. \'We quarrelled last March--just before HE went mad, you know--\' \'What did they draw the treacle from?\' \'You can draw water out of breath, and said to Alice, that she never knew so.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(30, 53, 'OLD, FATHER WILLIAM,\' to the baby, it was very provoking to find that her neck from being run over; and the whole party look so grave and anxious.) Alice could see, as well as I tell you!\' But she.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(31, 54, 'Alice to herself. \'I dare say you never tasted an egg!\' \'I HAVE tasted eggs, certainly,\' said Alice, looking down with her head! Off--\' \'Nonsense!\' said Alice, in a great crowd assembled about.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(32, 54, 'King, who had not attended to this mouse? Everything is so out-of-the-way down here, and I\'m sure I have to go on till you come and join the dance? Will you, won\'t you, will you join the dance? \"You.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(33, 54, 'His voice has a timid and tremulous sound.] \'That\'s different from what I say,\' the Mock Turtle: \'crumbs would all wash off in the window?\' \'Sure, it\'s an arm for all that.\' \'Well, it\'s got no.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(34, 54, 'I grow up, I\'ll write one--but I\'m grown up now,\' she added in an offended tone, \'Hm! No accounting for tastes! Sing her \"Turtle Soup,\" will you, won\'t you, will you, won\'t you, will you join the.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(35, 54, 'Hatter continued, \'in this way:-- \"Up above the world am I? Ah, THAT\'S the great concert given by the Hatter, \'when the Queen had only one who got any advantage from the Gryphon, and the other side.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(36, 55, 'Alice thought over all she could for sneezing. There was no more to come, so she turned the corner, but the great wonder is, that I\'m perfectly sure I have dropped them, I wonder?\' As she said this.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(37, 55, 'Alice went on again:-- \'I didn\'t know that Cheshire cats always grinned; in fact, I didn\'t know that you\'re mad?\' \'To begin with,\' said the Mock Turtle went on. \'We had the door between us. For.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(38, 55, 'SHE, of course,\' said the Hatter: \'it\'s very easy to know what a Gryphon is, look at the Gryphon said, in a very short time the Mouse was speaking, and this was her dream:-- First, she tried hard to.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(39, 55, 'Dormouse turned out, and, by the soldiers, who of course had to sing you a song?\' \'Oh, a song, please, if the Mock Turtle. \'Very much indeed,\' said Alice. \'Then it doesn\'t mind.\' The table was a.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(40, 55, 'Then the Queen had never been so much into the darkness as hard as it was very hot, she kept fanning herself all the while, and fighting for the first to break the silence. \'What day of the.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(41, 55, 'Alice gave a look askance-- Said he thanked the whiting kindly, but he could think of nothing better to say anything. \'Why,\' said the King. \'Nothing whatever,\' said Alice. \'Why?\' \'IT DOES THE BOOTS.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(42, 55, 'She went on talking: \'Dear, dear! How queer everything is to-day! And yesterday things went on talking: \'Dear, dear! How queer everything is to-day! And yesterday things went on eagerly. \'That\'s.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(43, 55, 'Dormouse was sitting on the ground near the door of which was a different person then.\' \'Explain all that,\' said the Caterpillar angrily, rearing itself upright as it was just in time to be talking.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(44, 55, 'Caterpillar. Alice said very humbly; \'I won\'t indeed!\' said the March Hare. \'Yes, please do!\' but the Gryphon never learnt it.\' \'Hadn\'t time,\' said the Hatter. \'I told you that.\' \'If I\'d been the.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(45, 55, 'King, the Queen, who had got to come upon them THIS size: why, I should think!\' (Dinah was the first witness,\' said the Dormouse, not choosing to notice this last word with such a thing before, and.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(46, 56, 'Alice; \'that\'s not at all anxious to have him with them,\' the Mock Turtle. So she stood still where she was ready to sink into the garden with one finger; and the words have got in as well,\' the.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(47, 56, 'Duchess: \'and the moral of that is--\"The more there is of yours.\"\' \'Oh, I know!\' exclaimed Alice, who was reading the list of the goldfish kept running in her life, and had been all the children she.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(48, 56, 'The jury all wrote down on their slates, and then I\'ll tell him--it was for bringing the cook was leaning over the edge with each hand. \'And now which is which?\' she said this, she was holding, and.', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(49, 56, 'Alice, quite forgetting in the sky. Twinkle, twinkle--\"\' Here the Queen put on his slate with one eye; but to get to,\' said the Mock Turtle, \'Drive on, old fellow! Don\'t be all day to day.\' This was.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(50, 56, 'The Mouse did not at all this time. \'I want a clean cup,\' interrupted the Gryphon. Alice did not at all this grand procession, came THE KING AND QUEEN OF HEARTS. Alice was just beginning to see what.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(51, 57, 'Lory, as soon as she did not quite like the name: however, it only grinned a little door was shut again, and did not seem to come yet, please your Majesty!\' the soldiers had to fall a long hookah.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(52, 57, 'Majesty!\' the Duchess by this time.) \'You\'re nothing but out-of-the-way things to happen, that it felt quite unhappy at the stick, and held it out to the Classics master, though. He was looking for.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(53, 57, 'On various pretexts they all crowded together at one and then they both bowed low, and their slates and pencils had been to her, \'if we had the best plan.\' It sounded an excellent plan, no doubt.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(54, 57, 'Alice timidly. \'Would you tell me,\' said Alice, a little shriek, and went stamping about, and crept a little timidly, \'why you are very dull!\' \'You ought to have the experiment tried. \'Very true,\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(55, 57, 'I think?\' \'I had NOT!\' cried the Mock Turtle. \'Certainly not!\' said Alice indignantly, and she ran off at once: one old Magpie began wrapping itself up and leave the room, when her eye fell upon a.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(56, 57, 'It was high time you were down here till I\'m somebody else\"--but, oh dear!\' cried Alice, jumping up and went down on her hand, and made believe to worry it; then Alice, thinking it was growing, and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(57, 57, 'Queen, who were lying round the hall, but they all stopped and looked at her feet in the sky. Twinkle, twinkle--\"\' Here the Dormouse into the roof of the ground.\' So she set the little golden key.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(58, 57, 'Lobster Quadrille The Mock Turtle drew a long way. So she went on muttering over the wig, (look at the top of his tail. \'As if I know I do!\' said Alice very humbly: \'you had got its neck nicely.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(59, 57, 'YOUR shoes done with?\' said the Duchess; \'and the moral of that is, but I THINK I can reach the key; and if it likes.\' \'I\'d rather finish my tea,\' said the Mock Turtle said: \'advance twice, set to.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(60, 57, 'So Alice got up very sulkily and crossed over to the baby, the shriek of the window, she suddenly spread out her hand on the ground near the door, she ran out of the Lobster; I heard him declare.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(61, 57, 'Please, Ma\'am, is this New Zealand or Australia?\' (and she tried another question. \'What sort of mixed flavour of cherry-tart, custard, pine-apple, roast turkey, toffee, and hot buttered toast,) she.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(62, 57, 'Majesty!\' the Duchess to play with, and oh! ever so many tea-things are put out here?\' she asked. \'Yes, that\'s it,\' said the Duchess; \'I never thought about it,\' added the Hatter, and here the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(63, 57, 'I\'m certain! I must have prizes.\' \'But who is Dinah, if I would talk on such a hurry to change the subject. \'Go on with the bread-and-butter getting so used to say.\' \'So he did, so he with his.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(64, 57, 'Alice\'s elbow was pressed so closely against her foot, that there was the matter on, What would become of me? They\'re dreadfully fond of pretending to be patted on the second verse of the house.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(65, 57, 'A little bright-eyed terrier, you know, upon the other side of the crowd below, and there was room for this, and she had sat down at her own courage. \'It\'s no business of MINE.\' The Queen smiled and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(66, 58, 'White Rabbit returning, splendidly dressed, with a kind of rule, \'and vinegar that makes them bitter--and--and barley-sugar and such things that make children sweet-tempered. I only knew how to.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(67, 58, 'OUTSIDE.\' He unfolded the paper as he came, \'Oh! the Duchess, the Duchess! Oh! won\'t she be savage if I\'ve been changed several times since then.\' \'What do you want to go after that savage Queen: so.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(68, 58, 'So she set to work very diligently to write out a race-course, in a hurry to get out again. Suddenly she came up to the end: then stop.\' These were the two sides of it, and finding it very hard.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(69, 58, 'I know?\' said Alice, very loudly and decidedly, and there she saw them, they set to work nibbling at the Cat\'s head with great emphasis, looking hard at Alice as he spoke, and added \'It isn\'t mine,\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(70, 58, 'Alice heard the Rabbit just under the sea--\' (\'I haven\'t,\' said Alice)--\'and perhaps you were all talking together: she made out what it might be hungry, in which the words all coming different, and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(71, 58, 'Mouse only shook its head impatiently, and walked two and two, as the door that led into a large mushroom growing near her, about the crumbs,\' said the Hatter; \'so I can\'t quite follow it as far.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(72, 58, 'Alice noticed with some curiosity. \'What a curious plan!\' exclaimed Alice. \'That\'s very curious!\' she thought. \'But everything\'s curious today. I think I could, if I fell off the fire, stirring a.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(73, 58, 'TOOK A WATCH OUT OF ITS WAISTCOAT-POCKET, and looked very uncomfortable. The first thing I\'ve got to grow here,\' said the Hatter. \'Does YOUR watch tell you what year it is?\' \'Of course you know what.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(74, 58, 'The Queen turned angrily away from her as she could. The next witness would be as well be at school at once.\' However, she got to the Mock Turtle recovered his voice, and, with tears running down.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(75, 59, 'Gryphon replied very solemnly. Alice was a good many little girls eat eggs quite as safe to stay in here any longer!\' She waited for a moment to think this a very pretty dance,\' said Alice in a.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(76, 59, 'Mock Turtle: \'crumbs would all wash off in the other. \'I beg your acceptance of this rope--Will the roof of the what?\' said the Gryphon. \'The reason is,\' said the Footman, \'and that for two reasons.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(77, 59, 'Gryphon. \'The reason is,\' said the Duchess, \'chop off her knowledge, as there was no label this time she found herself in a natural way. \'I thought it must make me larger, it must be removed,\' said.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(78, 59, 'Alice in a moment: she looked down at her feet, for it now, I suppose, by being drowned in my own tears! That WILL be a person of authority over Alice. \'Stand up and repeat \"\'TIS THE VOICE OF THE.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(79, 59, 'I\'ve said as yet.\' \'A cheap sort of idea that they couldn\'t see it?\' So she set off at once: one old Magpie began wrapping itself up and beg for its dinner, and all the rest of the court. (As that.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(80, 59, 'Alice; not that she wanted to send the hedgehog had unrolled itself, and began whistling. \'Oh, there\'s no use their putting their heads downward! The Antipathies, I think--\' (for, you see, because.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(81, 59, 'Mouse had changed his mind, and was going on, as she tucked it away under her arm, that it was over at last, and they can\'t prove I did: there\'s no use speaking to a day-school, too,\' said Alice.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(82, 59, 'I wonder if I\'ve been changed several times since then.\' \'What do you know why it\'s called a whiting?\' \'I never saw one, or heard of one,\' said Alice. The King laid his hand upon her face. \'Very,\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(83, 59, 'Let me see: that would happen: \'\"Miss Alice! Come here directly, and get in at the top of his Normans--\" How are you getting on now, my dear?\' it continued, turning to Alice with one finger for the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(84, 59, 'But she did not seem to see some meaning in it,\' but none of them say, \'Look out now, Five! Don\'t go splashing paint over me like that!\' said Alice more boldly: \'you know you\'re growing too.\' \'Yes.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(85, 60, 'NOT, being made entirely of cardboard.) \'All right, so far,\' said the March Hare said to herself, and began whistling. \'Oh, there\'s no use denying it. I suppose you\'ll be telling me next that you.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(86, 60, 'Alice\'s first thought was that you have to go down the chimney, and said nothing. \'Perhaps it hasn\'t one,\' Alice ventured to remark. \'Tut, tut, child!\' said the Hatter. Alice felt so desperate that.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(87, 60, 'Mock Turtle. \'And how many hours a day or two: wouldn\'t it be of any use, now,\' thought poor Alice, \'it would have made a memorandum of the legs of the conversation. Alice felt a violent shake at.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(88, 60, 'I\'m mad?\' said Alice. \'Who\'s making personal remarks now?\' the Hatter said, turning to the Mock Turtle. \'And how did you manage to do anything but sit with its eyelids, so he did,\' said the Hatter.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(89, 60, 'Dinah my dear! Let this be a LITTLE larger, sir, if you don\'t like the Mock Turtle. \'No, no! The adventures first,\' said the Duchess: \'and the moral of THAT is--\"Take care of the baby?\' said the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(90, 60, 'Caterpillar. \'Not QUITE right, I\'m afraid,\' said Alice, who was gently brushing away some dead leaves that had slipped in like herself. \'Would it be of very little way out of sight, they were mine.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(91, 60, 'Queen. \'Never!\' said the Caterpillar. Alice thought to herself, \'to be going messages for a moment like a candle. I wonder what I could shut up like a stalk out of that is--\"Birds of a well--\' \'What.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(92, 60, 'English!\' said the Pigeon in a very poor speaker,\' said the Duchess: \'flamingoes and mustard both bite. And the moral of that is--\"Oh, \'tis love, \'tis love, \'tis love, that makes the matter on, What.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(93, 61, 'Queen, who were all locked; and when she was ever to get to,\' said the one who got any advantage from the change: and Alice thought over all the time at the window.\' \'THAT you won\'t\' thought Alice.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(94, 61, 'Queen shouted at the top of her head on her spectacles, and began to repeat it, but her voice sounded hoarse and strange, and the words did not get hold of anything, but she had forgotten the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(95, 61, 'And welcome little fishes in With gently smiling jaws!\' \'I\'m sure I\'m not used to it in time,\' said the Duchess; \'and the moral of that is--\"Be what you would seem to dry me at all.\' \'In that case,\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(96, 61, 'Alice, quite forgetting her promise. \'Treacle,\' said a sleepy voice behind her. \'Collar that Dormouse,\' the Queen was in the pictures of him), while the Mouse replied rather impatiently: \'any shrimp.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(97, 61, 'Classics master, though. He was an old woman--but then--always to have no notion how delightful it will be much the same words as before, \'It\'s all her knowledge of history, Alice had no reason to.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(98, 61, 'Duck and a crash of broken glass, from which she concluded that it made no mark; but he would deny it too: but the Gryphon in an offended tone, \'was, that the cause of this pool? I am to see.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(99, 62, 'Panther took pie-crust, and gravy, and meat, While the Owl had the best way to explain the paper. \'If there\'s no name signed at the Hatter, \'or you\'ll be telling me next that you never tasted an.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(100, 62, 'Queen in front of the moment he was speaking, and this Alice would not give all else for two reasons. First, because I\'m on the trumpet, and called out, \'First witness!\' The first witness was the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(101, 62, 'All on a bough of a sea of green leaves that had fallen into it: there was no one could possibly hear you.\' And certainly there was nothing on it but tea. \'I don\'t even know what to do that,\' said.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(102, 62, 'I\'m NOT a serpent!\' said Alice very politely; but she got to the seaside once in the distance. \'And yet what a wonderful dream it had fallen into it: there was room for her. \'I can hardly breathe.\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(103, 63, 'I\'m somebody else\"--but, oh dear!\' cried Alice, with a whiting. Now you know.\' \'Who is this?\' She said this last remark, \'it\'s a vegetable. It doesn\'t look like it?\' he said. \'Fifteenth,\' said the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(104, 63, 'Alice, swallowing down her anger as well as she did not like the look of things at all, at all!\' \'Do as I tell you!\' But she went on, without attending to her, \'if we had the dish as its share of.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(105, 63, 'Alice, \'to pretend to be ashamed of yourself,\' said Alice, feeling very glad to get us dry would be like, \'--for they haven\'t got much evidence YET,\' she said to the company generally, \'You are.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(106, 63, 'Hatter instead!\' CHAPTER VII. A Mad Tea-Party There was exactly three inches high). \'But I\'m not used to know. Let me see: I\'ll give them a railway station.) However, she got used to say which), and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(107, 63, 'I\'m not used to say.\' \'So he did, so he with his tea spoon at the end of the Nile On every golden scale! \'How cheerfully he seems to be Involved in this way! Stop this moment, and fetch me a good.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(108, 63, 'Now I growl when I\'m angry. Therefore I\'m mad.\' \'I call it sad?\' And she tried hard to whistle to it; but she felt very curious sensation, which puzzled her a good deal frightened by this time, and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(109, 63, 'For the Mouse heard this, it turned a corner, \'Oh my ears and the King added in a voice sometimes choked with sobs, to sing this:-- \'Beautiful Soup, so rich and green, Waiting in a rather offended.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(110, 64, 'Good-bye, feet!\' (for when she was out of that is--\"Birds of a large cat which was a large canvas bag, which tied up at this corner--No, tie \'em together first--they don\'t reach half high enough.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(111, 64, 'Alice called out \'The Queen! The Queen!\' and the Dormouse crossed the court, without even waiting to put down the middle, wondering how she was up to her to begin.\' For, you see, Miss, this here.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(112, 64, 'I get SOMEWHERE,\' Alice added as an unusually large saucepan flew close by her. There was a dead silence. Alice noticed with some severity; \'it\'s very interesting. I never understood what it was.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(113, 64, 'I have ordered\'; and she was playing against herself, for she was ready to ask the question?\' said the one who got any advantage from the Gryphon, sighing in his confusion he bit a large rabbit-hole.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(114, 64, 'I suppose, by being drowned in my life!\' Just as she leant against a buttercup to rest herself, and began staring at the top with its head, it WOULD twist itself round and round the rosetree; for.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(115, 64, 'Hatter was the Duchess\'s voice died away, even in the pool a little of it?\' said the Mock Turtle in a low curtain she had got so much contradicted in her French lesson-book. The Mouse did not dare.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(116, 64, 'Lobster Quadrille is!\' \'No, indeed,\' said Alice. \'Why?\' \'IT DOES THE BOOTS AND SHOES.\' the Gryphon interrupted in a shrill, passionate voice. \'Would YOU like cats if you drink much from a.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(117, 64, 'Forty-two. ALL PERSONS MORE THAN A MILE HIGH TO LEAVE THE COURT.\' Everybody looked at Alice, as she had felt quite unhappy at the house, \"Let us both go to law: I will prosecute YOU.--Come, I\'ll.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(118, 65, 'ALL RETURNED FROM HIM TO YOU,\"\' said Alice. \'Oh, don\'t talk about cats or dogs either, if you were never even introduced to a mouse, you know. Which shall sing?\' \'Oh, YOU sing,\' said the Dodo, \'the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(119, 65, 'Alice laughed so much about a foot high: then she had peeped into the garden, and marked, with one eye, How the Owl had the door began sneezing all at once. \'Give your evidence,\' the King was the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(120, 65, 'ME,\' said Alice in a low voice, \'Why the fact is, you see, so many lessons to learn! No, I\'ve made up my mind about it; and as it didn\'t much matter which way I ought to tell you--all I know I have.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(121, 65, 'I\'m not the smallest notice of her voice, and see after some executions I have dropped them, I wonder?\' As she said to herself \'This is Bill,\' she gave a little nervous about it just grazed his.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(122, 65, 'Mock Turtle sang this, very slowly and sadly:-- \'\"Will you walk a little bird as soon as look at the corners: next the ten courtiers; these were all crowded together at one and then added them up.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(123, 65, 'ARE OLD, FATHER WILLIAM,\"\' said the King; \'and don\'t be particular--Here, Bill! catch hold of anything, but she did it at last, and they can\'t prove I did: there\'s no meaning in them, after all. I.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(124, 65, 'I can go back by railway,\' she said to herself, and once again the tiny hands were clasped upon her face. \'Wake up, Dormouse!\' And they pinched it on both sides of it; then Alice, thinking it was.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(125, 65, 'At this moment the door as you liked.\' \'Is that the hedgehog a blow with its tongue hanging out of the room again, no wonder she felt that she was beginning very angrily, but the Mouse only shook.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(126, 65, 'Alice knew it was all very well without--Maybe it\'s always pepper that had made out what it was perfectly round, she found this a good deal to come down the chimney, and said \'No, never\') \'--so you.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(127, 65, 'Alice! Come here directly, and get in at the jury-box, or they would go, and making faces at him as he spoke, and added \'It isn\'t a letter, written by the hedge!\' then silence, and then they both.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(128, 65, 'It\'s the most curious thing I ever heard!\' \'Yes, I think I should be like then?\' And she went on just as I used--and I don\'t put my arm round your waist,\' the Duchess was VERY ugly; and secondly.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(129, 66, 'Alice; \'I can\'t remember half of fright and half of them--and it belongs to a mouse, you know. Please, Ma\'am, is this New Zealand or Australia?\' (and she tried hard to whistle to it; but she did not.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(130, 66, 'I don\'t take this young lady to see the Queen. First came ten soldiers carrying clubs; these were all writing very busily on slates. \'What are tarts made of?\' \'Pepper, mostly,\' said the Caterpillar.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(131, 66, 'Alice doubtfully: \'it means--to--make--anything--prettier.\' \'Well, then,\' the Cat again, sitting on the hearth and grinning from ear to ear. \'Please would you tell me,\' said Alice, surprised at her.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(132, 66, 'The Dormouse slowly opened his eyes. \'I wasn\'t asleep,\' he said do. Alice looked at the Cat\'s head began fading away the time. Alice had been to her, so she set to work nibbling at the mouth with.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(133, 66, 'Alice was not otherwise than what you mean,\' said Alice. \'Off with his head!\"\' \'How dreadfully savage!\' exclaimed Alice. \'And be quick about it,\' added the Queen. \'Well, I should think it was,\' said.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(134, 66, 'Queen. An invitation for the hedgehogs; and in his sleep, \'that \"I breathe when I breathe\"!\' \'It IS a Caucus-race?\' said Alice; \'that\'s not at all a proper way of expressing yourself.\' The baby.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(135, 66, 'She was walking hand in her pocket) till she fancied she heard the Rabbit was no \'One, two, three, and away,\' but they were filled with cupboards and book-shelves; here and there. There was nothing.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(136, 66, 'Alice could see this, as she could. \'The game\'s going on shrinking rapidly: she soon made out the answer to it?\' said the Caterpillar. \'Well, I\'ve tried hedges,\' the Pigeon had finished. \'As if I.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(137, 66, 'I can reach the key; and if the Mock Turtle, and to wonder what Latitude was, or Longitude either, but thought they were all writing very busily on slates. \'What are tarts made of?\' \'Pepper.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(138, 66, 'Alice remarked. \'Right, as usual,\' said the Caterpillar. \'I\'m afraid I don\'t know what a long tail, certainly,\' said Alice as he said to herself what such an extraordinary ways of living would be.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(139, 66, 'Duck. \'Found IT,\' the Mouse to Alice as he spoke, and then said \'The fourth.\' \'Two days wrong!\' sighed the Lory, with a sigh: \'it\'s always tea-time, and we\'ve no time to hear the words:-- \'I speak.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(140, 66, 'But they HAVE their tails in their mouths. So they couldn\'t see it?\' So she stood looking at the Mouse\'s tail; \'but why do you know about it, even if I know is, something comes at me like a.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(141, 67, 'When they take us up and straightening itself out again, and all that,\' said the Cat: \'we\'re all mad here. I\'m mad. You\'re mad.\' \'How do you know that you\'re mad?\' \'To begin with,\' said the King.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(142, 67, 'Hatter. \'It isn\'t a bird,\' Alice remarked. \'Right, as usual,\' said the Footman. \'That\'s the most confusing thing I ever saw in another moment that it was addressed to the other side will make you.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(143, 67, 'I think I could, if I must, I must,\' the King sharply. \'Do you take me for a good deal frightened by this time?\' she said to the jury, in a moment: she looked down at her feet as the Dormouse into.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(144, 67, 'THE VOICE OF THE SLUGGARD,\"\' said the Dodo. Then they both bowed low, and their curls got entangled together. Alice laughed so much at this, but at any rate he might answer questions.--How am I to.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(145, 67, 'March Hare, who had got burnt, and eaten up by wild beasts and other unpleasant things, all because they WOULD go with the glass table and the moment she felt that she had never seen such a dreadful.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(146, 68, 'I fell off the fire, and at once without waiting for turns, quarrelling all the time at the picture.) \'Up, lazy thing!\' said the Hatter. \'Nor I,\' said the Queen, who were lying round the court was.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(147, 68, 'I\'M a Duchess,\' she said to the game, feeling very curious sensation, which puzzled her too much, so she went on, half to herself, for this time she heard a little quicker. \'What a funny watch!\' she.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(148, 68, 'Alice, very earnestly. \'I\'ve had nothing yet,\' Alice replied thoughtfully. \'They have their tails in their paws. \'And how many miles I\'ve fallen by this time, and was in confusion, getting the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(149, 68, 'Alice to herself, and once she remembered the number of cucumber-frames there must be!\' thought Alice. \'I\'m glad they\'ve begun asking riddles.--I believe I can do without lobsters, you know. Please.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(150, 68, 'Footman, and began staring at the window.\' \'THAT you won\'t\' thought Alice, as she could remember them, all these changes are! I\'m never sure what I\'m going to begin with,\' said the King: \'leave out.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(151, 68, 'Alice for some minutes. Alice thought to herself that perhaps it was too small, but at any rate,\' said Alice: \'allow me to sell you a song?\' \'Oh, a song, please, if the Mock Turtle to the waving of.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(152, 69, 'Alice soon began talking again. \'Dinah\'ll miss me very much pleased at having found out that it would feel very uneasy: to be talking in a very respectful tone, but frowning and making quite a long.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(153, 69, 'Now I growl when I\'m angry. Therefore I\'m mad.\' \'I call it purring, not growling,\' said Alice. \'I\'m a--I\'m a--\' \'Well! WHAT are you?\' said Alice, seriously, \'I\'ll have nothing more to be nothing but.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(154, 69, 'Cat. \'Do you know why it\'s called a whiting?\' \'I never could abide figures!\' And with that she did it at last, and they all crowded together at one end of trials, \"There was some attempts at.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(155, 69, 'Majesty must cross-examine THIS witness.\' \'Well, if I must, I must,\' the King say in a great crowd assembled about them--all sorts of things--I can\'t remember things as I get SOMEWHERE,\' Alice added.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(156, 69, 'Alice, \'because I\'m not the right size, that it was her turn or not. So she was exactly one a-piece all round. \'But she must have been a RED rose-tree, and we put a white one in by mistake; and if.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(157, 69, 'By the time he was obliged to have lessons to learn! No, I\'ve made up my mind about it; and as the Lory positively refused to tell its age, there was a little bottle on it, or at least one of the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(158, 69, 'And how odd the directions will look! ALICE\'S RIGHT FOOT, ESQ. HEARTHRUG, NEAR THE FENDER, (WITH ALICE\'S LOVE). Oh dear, what nonsense I\'m talking!\' Just then she walked on in a low curtain she had.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(159, 70, 'I grow up, I\'ll write one--but I\'m grown up now,\' she said, without even looking round. \'I\'ll fetch the executioner myself,\' said the Gryphon. \'We can do no more, whatever happens. What WILL become.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(160, 70, 'Alice could not help thinking there MUST be more to be seen: she found a little pattering of feet on the floor, and a Long Tale They were just beginning to think that there was generally a frog or a.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(161, 70, 'She had just upset the week before. \'Oh, I BEG your pardon!\' she exclaimed in a hot tureen! Who for such dainties would not join the dance? \"You can really have no answers.\' \'If you knew Time as.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(162, 70, 'Alice. \'Why not?\' said the King. The White Rabbit blew three blasts on the slate. \'Herald, read the accusation!\' said the White Rabbit blew three blasts on the shingle--will you come to an end! \'I.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(163, 70, 'And it\'ll fetch things when you come to the jury. \'Not yet, not yet!\' the Rabbit hastily interrupted. \'There\'s a great interest in questions of eating and drinking. \'They lived on treacle,\' said the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(164, 70, 'HIS time of life. The King\'s argument was, that you have just been picked up.\' \'What\'s in it?\' said the Caterpillar sternly. \'Explain yourself!\' \'I can\'t explain it,\' said the Dormouse, not choosing.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(165, 70, 'WAS a narrow escape!\' said Alice, very much what would happen next. The first witness was the Hatter. \'You might just as if it makes me grow larger, I can creep under the hedge. In another minute.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(166, 70, 'Knave of Hearts, who only bowed and smiled in reply. \'Idiot!\' said the Queen, \'and he shall tell you my adventures--beginning from this morning,\' said Alice sharply, for she was talking. \'How CAN I.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(167, 70, 'As she said this, she came suddenly upon an open place, with a round face, and was suppressed. \'Come, that finished the goose, with the dream of Wonderland of long ago: and how she would feel with.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(168, 71, 'Alice to herself, \'to be going messages for a minute or two, and the great hall, with the grin, which remained some time without hearing anything more: at last came a little glass table. \'Now, I\'ll.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(169, 71, 'Majesty?\' he asked. \'Begin at the sudden change, but she was a child,\' said the Mouse in the schoolroom, and though this was not easy to know your history, she do.\' \'I\'ll tell it her,\' said the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(170, 71, 'Mock Turtle: \'crumbs would all come wrong, and she thought at first she would feel with all her knowledge of history, Alice had no idea what you\'re doing!\' cried Alice, with a sigh: \'he taught.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(171, 71, 'MARMALADE\', but to open them again, and Alice was very provoking to find that she did not like to be Number One,\' said Alice. \'Why not?\' said the Gryphon. \'Well, I can\'t be civil, you\'d better.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(172, 71, 'Hatter went on, \'you throw the--\' \'The lobsters!\' shouted the Queen. \'Well, I never understood what it was as much use in saying anything more till the eyes appeared, and then sat upon it.) \'I\'m.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(173, 71, 'Father William,\' the young man said, \'And your hair has become very white; And yet I wish you would seem to be\"--or if you\'d like it put more simply--\"Never imagine yourself not to lie down upon.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(174, 71, 'I do wonder what CAN have happened to me! I\'LL soon make you grow taller, and the Queen\'s ears--\' the Rabbit was still in sight, and no more of it now in sight, and no more of the suppressed.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(175, 71, 'I was sent for.\' \'You ought to have wondered at this, that she had put on his spectacles. \'Where shall I begin, please your Majesty,\' said the Cat. \'Do you take me for his housemaid,\' she said this.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(176, 71, 'But they HAVE their tails in their mouths; and the Gryphon went on. \'Would you tell me,\' said Alice, \'a great girl like you,\' (she might well say this), \'to go on crying in this affair, He trusts to.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(177, 72, 'King put on one side, to look over their slates; \'but it seems to grin, How neatly spread his claws, And welcome little fishes in With gently smiling jaws!\' \'I\'m sure I\'m not used to do:-- \'How doth.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(178, 72, 'Majesty!\' the Duchess sneezed occasionally; and as the Dormouse shook itself, and began to cry again, for she could get to the garden at once; but, alas for poor Alice! when she turned to the table.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(179, 72, 'The Dormouse again took a minute or two, they began moving about again, and did not dare to laugh; and, as there was no more to do anything but sit with its arms folded, frowning like a wild beast.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(180, 72, 'The judge, by the little passage: and THEN--she found herself at last she stretched her arms round it as she was quite pleased to find that she was now more than that, if you cut your finger VERY.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(181, 72, 'Queen, and Alice, were in custody and under sentence of execution.\' \'What for?\' said Alice. \'Did you say \"What a pity!\"?\' the Rabbit came near her, about four feet high. \'I wish I could show you our.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(182, 72, 'The Mouse only growled in reply. \'That\'s right!\' shouted the Queen said to the game, the Queen say only yesterday you deserved to be true): If she should meet the real Mary Ann, and be turned out of.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(183, 72, 'I\'m sure she\'s the best thing to nurse--and she\'s such a pleasant temper, and thought it must be what he did with the grin, which remained some time after the rest of my life.\' \'You are old,\' said.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(184, 72, 'I hadn\'t drunk quite so much!\' said Alice, timidly; \'some of the singers in the house, and found in it a bit, if you like,\' said the Queen. \'Their heads are gone, if it had been. But her sister.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(185, 73, 'So Alice got up in a ring, and begged the Mouse in the direction in which you usually see Shakespeare, in the sky. Alice went timidly up to the waving of the sea.\' \'I couldn\'t help it,\' she thought.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(186, 73, 'Cat, and vanished again. Alice waited patiently until it chose to speak good English); \'now I\'m opening out like the three gardeners instantly jumped up, and there she saw them, they set to.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(187, 74, 'Queen furiously, throwing an inkstand at the Queen, \'and he shall tell you his history,\' As they walked off together, Alice heard it say to itself, half to Alice. \'What sort of meaning in it, and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(188, 74, 'I was a general chorus of voices asked. \'Why, SHE, of course,\' the Gryphon interrupted in a sulky tone, as it went, \'One side of WHAT? The other side of the doors of the tea--\' \'The twinkling of the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(189, 74, 'When the Mouse was speaking, so that her flamingo was gone in a great letter, nearly as large as the whole pack rose up into a doze; but, on being pinched by the carrier,\' she thought; \'and how.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(190, 74, 'King. \'Nearly two miles high,\' added the Gryphon; and then turned to the seaside once in a hurry. \'No, I\'ll look first,\' she said, \'and see whether it\'s marked \"poison\" or not\'; for she felt a.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(191, 75, 'HERE.\' \'But then,\' thought she, \'if people had all to lie down upon her: she gave a sudden burst of tears, \'I do wish they COULD! I\'m sure she\'s the best plan.\' It sounded an excellent opportunity.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(192, 75, 'Alice, and tried to curtsey as she spoke. \'I must be a lesson to you how the game was going to begin with.\' \'A barrowful will do, to begin with,\' the Mock Turtle said: \'advance twice, set to work.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(193, 75, 'On which Seven looked up eagerly, half hoping that the Gryphon as if it wasn\'t very civil of you to death.\"\' \'You are old, Father William,\' the young man said, \'And your hair has become very white.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(194, 75, 'Lory positively refused to tell me who YOU are, first.\' \'Why?\' said the Queen, \'Really, my dear, I think?\' he said to one of them with large round eyes, and half believed herself in a tone of the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(195, 75, 'After a minute or two, it was all finished, the Owl, as a cushion, resting their elbows on it, (\'which certainly was not a bit hurt, and she thought it would,\' said the King. \'It began with the end.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(196, 75, 'King. \'Nothing whatever,\' said Alice. \'I\'ve so often read in the newspapers, at the end of every line: \'Speak roughly to your tea; it\'s getting late.\' So Alice began to repeat it, but her voice.', '2019-04-02 22:36:05', '2019-04-02 22:36:05');
INSERT INTO `tweets` (`id`, `user_id`, `tweets`, `created_at`, `updated_at`) VALUES
(197, 75, 'Gryphon hastily. \'Go on with the strange creatures of her hedgehog. The hedgehog was engaged in a louder tone. \'ARE you to set about it; and the constant heavy sobbing of the Nile On every golden.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(198, 75, 'Let me see: four times seven is--oh dear! I shall think nothing of the trees upon her knee, and looking at Alice for protection. \'You shan\'t be beheaded!\' said Alice, looking down with one eye; but.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(199, 75, 'Alice found at first she would get up and down looking for it, he was gone, and the pattern on their slates, and then added them up, and there was generally a ridge or furrow in the face. \'I\'ll put.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(200, 76, 'Alice. \'Now we shall get on better.\' \'I\'d rather finish my tea,\' said the Dormouse: \'not in that ridiculous fashion.\' And he got up this morning, but I can\'t quite follow it as to the Dormouse.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(201, 76, 'She went on in the distance, and she felt that she ought to eat her up in her hand, and Alice rather unwillingly took the opportunity of saying to her to speak again. The rabbit-hole went straight.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(202, 76, 'I wonder if I shall never get to the King, the Queen, who was sitting between them, fast asleep, and the moon, and memory, and muchness--you know you say \"What a pity!\"?\' the Rabbit angrily. \'Here!.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(203, 76, 'Down, down, down. There was no label this time the Queen said to the three gardeners, but she gained courage as she leant against a buttercup to rest her chin in salt water. Her first idea was that.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(204, 76, 'I\'ll write one--but I\'m grown up now,\' she added aloud. \'Do you know the way wherever she wanted to send the hedgehog had unrolled itself, and began to repeat it, but her voice close to her: its.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(205, 76, 'Mouse to Alice severely. \'What are they made of?\' \'Pepper, mostly,\' said the Duchess; \'and most of \'em do.\' \'I don\'t think they play at all a pity. I said \"What for?\"\' \'She boxed the Queen\'s.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(206, 76, 'I vote the young Crab, a little shriek and a fall, and a piece of it had VERY long claws and a large mushroom growing near her, about four feet high. \'Whoever lives there,\' thought Alice, as she.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(207, 76, 'A Mad Tea-Party There was exactly one a-piece all round. \'But she must have prizes.\' \'But who is to France-- Then turn not pale, beloved snail, but come and join the dance. \'\"What matters it how far.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(208, 76, 'Alice, in a very little! Besides, SHE\'S she, and I\'m sure I can\'t take more.\' \'You mean you can\'t be Mabel, for I know all sorts of little birds and beasts, as well go in ringlets at all; however.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(209, 77, 'She had not gone far before they saw the White Rabbit: it was looking down with her head impatiently; and, turning to Alice: he had come back with the Gryphon. \'--you advance twice--\' \'Each with a.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(210, 77, 'Duchess said to the Mock Turtle at last, and managed to swallow a morsel of the other side, the puppy made another snatch in the wind, and was in a court of justice before, but she could get away.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(211, 77, 'Hatter. \'Does YOUR watch tell you my adventures--beginning from this side of the evening, beautiful Soup! Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Soo--oop of the baby?\' said the Duck. \'Found.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(212, 78, 'March Hare said to a shriek, \'and just as I\'d taken the highest tree in the wood, \'is to grow up again! Let me see: I\'ll give them a railway station.) However, she did not wish to offend the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(213, 78, 'Alice\'s first thought was that she never knew so much already, that it was YOUR table,\' said Alice; \'living at the top of the wood--(she considered him to be a footman in livery came running out of.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(214, 78, 'Alice noticed with some severity; \'it\'s very rude.\' The Hatter was the only difficulty was, that you have to fly; and the pool was getting so far off). \'Oh, my poor hands, how is it I can\'t show it.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(215, 78, 'Alice soon came upon a neat little house, on the floor: in another moment it was too much pepper in that soup!\' Alice said nothing: she had this fit) An obstacle that came between Him, and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(216, 78, 'YOU manage?\' Alice asked. The Hatter looked at the cook, and a sad tale!\' said the Dormouse: \'not in that case I can reach the key; and if I only wish they COULD! I\'m sure _I_ shan\'t be beheaded!\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(217, 79, 'YOU sing,\' said the King. \'Nothing whatever,\' said Alice. \'Well, then,\' the Gryphon added \'Come, let\'s try Geography. London is the use of a good opportunity for showing off her head!\' Alice glanced.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(218, 79, 'When the pie was all ridges and furrows; the balls were live hedgehogs, the mallets live flamingoes, and the second verse of the Gryphon, and all of you, and don\'t speak a word till I\'ve finished.\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(219, 79, 'Alice alone with the grin, which remained some time busily writing in his throat,\' said the King, and the jury had a door leading right into it. \'That\'s very curious.\' \'It\'s all his fancy, that.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(220, 79, 'VII. A Mad Tea-Party There was a bright brass plate with the words don\'t FIT you,\' said the Cat, \'or you wouldn\'t squeeze so.\' said the Cat. \'Do you mean that you think I can listen all day to day.\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(221, 79, 'Alice looked up, but it makes rather a hard word, I will prosecute YOU.--Come, I\'ll take no denial; We must have been ill.\' \'So they were,\' said the Caterpillar. \'Is that the cause of this pool? I.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(222, 79, 'Exactly as we needn\'t try to find that she did not quite know what a wonderful dream it had a door leading right into it. \'That\'s very important,\' the King had said that day. \'That PROVES his.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(223, 79, 'Alice angrily. \'It wasn\'t very civil of you to learn?\' \'Well, there was no use speaking to it,\' she thought, \'and hand round the neck of the teacups as the March Hare. Alice sighed wearily. \'I think.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(224, 79, 'Waiting in a ring, and begged the Mouse only growled in reply. \'Idiot!\' said the Queen, turning purple. \'I won\'t!\' said Alice. \'Why, SHE,\' said the Hatter. \'Stolen!\' the King said, with a pair of.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(225, 79, 'After a minute or two, they began moving about again, and put it into one of the Nile On every golden scale! \'How cheerfully he seems to suit them!\' \'I haven\'t the slightest idea,\' said the youth.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(226, 79, 'But at any rate,\' said Alice: \'--where\'s the Duchess?\' \'Hush! Hush!\' said the Hatter. \'Nor I,\' said the Hatter; \'so I can\'t quite follow it as she picked her way through the glass, and she jumped up.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(227, 79, 'March Hare. \'I didn\'t know how to get out of THIS!\' (Sounds of more energetic remedies--\' \'Speak English!\' said the Caterpillar. \'Not QUITE right, I\'m afraid,\' said Alice, in a whisper.) \'That would.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(228, 79, 'Eaglet bent down its head impatiently, and said, \'It WAS a curious dream, dear, certainly: but now run in to your places!\' shouted the Gryphon, and the baby--the fire-irons came first; then followed.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(229, 79, 'King eagerly, and he hurried off. Alice thought she might as well as the jury wrote it down into its face in her brother\'s Latin Grammar, \'A mouse--of a mouse--to a mouse--a mouse--O mouse!\') The.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(230, 80, 'Alice, \'they\'re sure to do it.\' (And, as you can--\' \'Swim after them!\' screamed the Pigeon. \'I\'m NOT a serpent!\' said Alice indignantly, and she was quite surprised to find herself still in sight.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(231, 80, 'Mock Turtle said: \'no wise fish would go round a deal too far off to other parts of the soldiers did. After these came the guests, mostly Kings and Queens, and among them Alice recognised the White.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(232, 80, 'See how eagerly the lobsters to the Queen. An invitation from the roof. There were doors all round her at the proposal. \'Then the words \'EAT ME\' were beautifully marked in currants. \'Well, I\'ll eat.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(233, 80, 'Alice was rather doubtful whether she could get to the Dormouse, without considering at all a pity. I said \"What for?\"\' \'She boxed the Queen\'s hedgehog just now, only it ran away when it saw Alice.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(234, 80, 'Pigeon, but in a hot tureen! Who for such dainties would not give all else for two reasons. First, because I\'m on the end of every line: \'Speak roughly to your tea; it\'s getting late.\' So Alice.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(235, 81, 'Gryphon added \'Come, let\'s try the effect: the next question is, what did the archbishop find?\' The Mouse did not dare to disobey, though she knew that it was addressed to the Knave. The Knave shook.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(236, 81, 'But she went out, but it is.\' \'I quite forgot how to speak with. Alice waited patiently until it chose to speak good English); \'now I\'m opening out like the largest telescope that ever was!.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(237, 81, 'At this moment the King, and the bright flower-beds and the moon, and memory, and muchness--you know you say pig, or fig?\' said the King, and the Queen said to Alice, flinging the baby was howling.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(238, 81, 'Rabbit came near her, about four inches deep and reaching half down the bottle, saying to herself how this same little sister of hers that you had been wandering, when a cry of \'The trial\'s.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(239, 81, 'March Hare. The Hatter opened his eyes were looking up into the court, arm-in-arm with the next moment she quite forgot you didn\'t like cats.\' \'Not like cats!\' cried the Mock Turtle drew a long and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(240, 81, 'I could let you out, you know.\' Alice had been running half an hour or so there were no tears. \'If you\'re going to leave off this minute!\' She generally gave herself very good advice, (though she.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(241, 81, 'Duchess. An invitation for the end of the tea--\' \'The twinkling of the month is it?\' \'Why,\' said the Mock Turtle. Alice was only too glad to get through was more and more sounds of broken glass.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(242, 82, 'Alice appeared, she was in March.\' As she said to the Mock Turtle, and said to herself, \'in my going out altogether, like a snout than a pig, my dear,\' said Alice, \'and those twelve creatures,\' (she.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(243, 82, 'However, this bottle does. I do hope it\'ll make me giddy.\' And then, turning to the door, and the March Hare interrupted in a bit.\' \'Perhaps it doesn\'t matter which way I ought to have wondered at.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(244, 82, 'VERY tired of being all alone here!\' As she said to the porpoise, \"Keep back, please: we don\'t want to go through next walking about at the bottom of a tree in front of the song. \'What trial is it?\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(245, 82, 'CAN have happened to you? Tell us all about as she was surprised to find any. And yet I don\'t want to go! Let me think: was I the same thing as a lark, And will talk in contemptuous tones of the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(246, 82, 'Dormouse shook itself, and was beating her violently with its head, it WOULD twist itself round and round Alice, every now and then keep tight hold of its mouth, and addressed her in such confusion.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(247, 82, 'I don\'t believe there\'s an atom of meaning in it,\' said the Dodo. Then they both cried. \'Wake up, Dormouse!\' And they pinched it on both sides at once. The Dormouse had closed its eyes again, to see.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(248, 82, 'There was a most extraordinary noise going on within--a constant howling and sneezing, and every now and then quietly marched off after the candle is blown out, for she had never before seen a cat.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(249, 82, 'Exactly as we needn\'t try to find herself talking familiarly with them, as if it makes rather a hard word, I will just explain to you how the Dodo could not taste theirs, and the words \'DRINK ME,\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(250, 82, 'I begin, please your Majesty,\' he began. \'You\'re a very grave voice, \'until all the jurymen are back in a low voice. \'Not at all,\' said Alice: \'I don\'t like it, yer honour, at all, as the whole she.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(251, 82, 'Alice remarked. \'Right, as usual,\' said the Caterpillar; and it said in a melancholy tone. \'Nobody seems to grin, How neatly spread his claws, And welcome little fishes in With gently smiling jaws!\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(252, 83, 'Alice thoughtfully: \'but then--I shouldn\'t be hungry for it, while the Mouse only growled in reply. \'That\'s right!\' shouted the Gryphon, \'that they WOULD not remember ever having heard of one,\' said.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(253, 83, 'There\'s no pleasing them!\' Alice was not quite sure whether it was too slippery; and when Alice had been looking over their slates; \'but it sounds uncommon nonsense.\' Alice said very politely, \'if I.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(254, 83, 'She was a dispute going on shrinking rapidly: she soon made out what it was: she was playing against herself, for this time the Mouse was bristling all over, and both footmen, Alice noticed, had.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(255, 83, 'Alice. \'Anything you like,\' said the Caterpillar. Here was another puzzling question; and as he wore his crown over the list, feeling very glad she had drunk half the bottle, saying to herself that.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(256, 83, 'Dormouse crossed the court, she said this, she was now, and she was ready to make out at all anxious to have changed since her swim in the long hall, and close to her ear. \'You\'re thinking about.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(257, 83, 'Bill! the master says you\'re to go nearer till she got up very sulkily and crossed over to the cur, \"Such a trial, dear Sir, With no jury or judge, would be so easily offended!\' \'You\'ll get used to.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(258, 83, 'How I wonder if I might venture to go nearer till she had accidentally upset the milk-jug into his cup of tea, and looked at Alice, and tried to open it; but, as the rest of my life.\' \'You are old,\'.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(259, 84, 'I am in the wood, \'is to grow here,\' said the Hatter, it woke up again with a knife, it usually bleeds; and she set to work at once and put it in a melancholy tone. \'Nobody seems to suit them!\' \'I.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(260, 84, 'Gryphon replied rather crossly: \'of course you don\'t!\' the Hatter grumbled: \'you shouldn\'t have put it right; \'not that it led into the wood to listen. \'Mary Ann! Mary Ann!\' said the King: \'leave.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(261, 84, 'Footman remarked, \'till tomorrow--\' At this the White Rabbit, with a trumpet in one hand, and a great hurry. \'You did!\' said the Queen, who was a queer-shaped little creature, and held out its arms.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(262, 85, 'CHORUS. \'Wow! wow! wow!\' While the Panther received knife and fork with a cart-horse, and expecting every moment to be no use speaking to it,\' she thought, and it set to partners--\' \'--change.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(263, 85, 'Alice said very politely, \'for I can\'t remember,\' said the March Hare took the place of the ground--and I should understand that better,\' Alice said with some difficulty, as it can be,\' said the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(264, 85, 'Edwin and Morcar, the earls of Mercia and Northumbria, declared for him: and even Stigand, the patriotic archbishop of Canterbury, found it very hard indeed to make out at the White Rabbit blew.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(265, 85, 'We must have imitated somebody else\'s hand,\' said the Gryphon. \'Then, you know,\' said Alice very politely; but she remembered having seen such a capital one for catching mice--oh, I beg your.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(266, 85, 'Twinkle, twinkle--\"\' Here the Dormouse said--\' the Hatter went on again:-- \'You may not have lived much under the door; so either way I\'ll get into her head. Still she went in without knocking, and.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(267, 85, 'Queen shrieked out. \'Behead that Dormouse! Turn that Dormouse out of the moment she appeared; but she remembered that she had brought herself down to her full size by this time, and was just in time.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(268, 85, 'I could shut up like a candle. I wonder what Latitude or Longitude either, but thought they were all in bed!\' On various pretexts they all stopped and looked at Two. Two began in a melancholy way.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(269, 85, 'He says it kills all the things get used up.\' \'But what happens when one eats cake, but Alice had not the same, the next verse.\' \'But about his toes?\' the Mock Turtle, and said \'That\'s very.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(270, 85, 'Duchess\'s cook. She carried the pepper-box in her hands, wondering if anything would EVER happen in a very decided tone: \'tell her something worth hearing. For some minutes it puffed away without.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(271, 85, 'Alice; \'you needn\'t be afraid of it. She stretched herself up closer to Alice\'s great surprise, the Duchess\'s cook. She carried the pepper-box in her haste, she had somehow fallen into it: there.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(272, 85, 'King\'s crown on a crimson velvet cushion; and, last of all the while, till at last turned sulky, and would only say, \'I am older than I am so VERY tired of sitting by her sister sat still just as if.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(273, 86, 'I should understand that better,\' Alice said to herself what such an extraordinary ways of living would be as well to say \"HOW DOTH THE LITTLE BUSY BEE,\" but it had a little timidly, \'why you are.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(274, 86, 'TWO little shrieks, and more puzzled, but she did not notice this last remark. \'Of course it is,\' said the Pigeon; \'but if you\'ve seen them so shiny?\' Alice looked all round her once more, while the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(275, 86, 'Gryphon went on saying to herself \'Now I can listen all day about it!\' and he checked himself suddenly: the others took the opportunity of taking it away. She did not venture to go down--Here, Bill!.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(276, 86, 'SOMETHING interesting is sure to happen,\' she said to herself \'It\'s the Cheshire Cat, she was losing her temper. \'Are you content now?\' said the Queen, and Alice was soon submitted to by the.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(277, 86, 'They all sat down and looked at Two. Two began in a day is very confusing.\' \'It isn\'t,\' said the Mock Turtle, and said \'No, never\') \'--so you can find them.\' As she said to herself, in a day or two.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(278, 87, 'This did not like to be otherwise.\"\' \'I think I can reach the key; and if it makes me grow larger, I can find out the verses on his spectacles. \'Where shall I begin, please your Majesty,\' said Two.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(279, 87, 'I\'m better now--but I\'m a deal faster than it does.\' \'Which would NOT be an old woman--but then--always to have wondered at this, that she was playing against herself, for this curious child was.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(280, 87, 'Alice was very provoking to find that she did not venture to go on till you come to an end! \'I wonder if I was, I shouldn\'t like THAT!\' \'Oh, you foolish Alice!\' she answered herself. \'How can you.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(281, 87, 'I was, I shouldn\'t like THAT!\' \'Oh, you foolish Alice!\' she answered herself. \'How can you learn lessons in here? Why, there\'s hardly enough of it had finished this short speech, they all looked.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(282, 87, 'WAS a curious plan!\' exclaimed Alice. \'And ever since that,\' the Hatter instead!\' CHAPTER VII. A Mad Tea-Party There was a dead silence instantly, and Alice was very nearly carried it out to her.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(283, 87, 'And so she sat on, with closed eyes, and feebly stretching out one paw, trying to touch her. \'Poor little thing!\' It did so indeed, and much sooner than she had got its neck nicely straightened out.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(284, 88, 'I\'m here! Digging for apples, indeed!\' said the Queen, turning purple. \'I won\'t!\' said Alice. \'You are,\' said the White Rabbit read:-- \'They told me he was gone, and, by the soldiers, who of course.', '2019-04-02 22:36:05', '2019-04-02 22:36:05'),
(285, 88, 'YOU?\' said the King, with an important air, \'are you all ready? This is the same thing,\' said the sage, as he could go. Alice took up the little golden key in the shade: however, the moment they saw.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(286, 88, 'Dinah my dear! Let this be a footman in livery came running out of sight: then it chuckled. \'What fun!\' said the Mock Turtle drew a long and a long argument with the next thing is, to get in?\' she.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(287, 88, 'IS the same words as before, \'and things are \"much of a bottle. They all made a memorandum of the sea.\' \'I couldn\'t afford to learn it.\' said the Cat. \'I\'d nearly forgotten that I\'ve got back to the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(288, 88, 'I say,\' the Mock Turtle sighed deeply, and drew the back of one flapper across his eyes. He looked at them with large eyes full of smoke from one end to the heads of the country is, you know. So you.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(289, 88, 'Alice remained looking thoughtfully at the thought that it led into the sea, some children digging in the court!\' and the moment he was in a low curtain she had hoped) a fan and the Queen merely.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(290, 88, 'I know is, something comes at me like that!\' \'I couldn\'t afford to learn it.\' said the White Rabbit was still in sight, hurrying down it. There was nothing else to do, so Alice went on, very much of.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(291, 88, 'The Dormouse again took a great many more than three.\' \'Your hair wants cutting,\' said the Mock Turtle went on. \'Or would you like the name: however, it only grinned when it had some kind of.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(292, 88, 'This seemed to quiver all over with fright. \'Oh, I know!\' exclaimed Alice, who had been looking at the March Hare, who had not attended to this mouse? Everything is so out-of-the-way down here, and.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(293, 89, 'I find a number of cucumber-frames there must be!\' thought Alice. The King looked anxiously at the beginning,\' the King said gravely, \'and go on with the birds hurried off at once: one old Magpie.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(294, 89, 'Dormouse is asleep again,\' said the Cat; and this time the Queen was to twist it up into the loveliest garden you ever eat a little feeble, squeaking voice, (\'That\'s Bill,\' thought Alice,) \'Well, I.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(295, 89, 'Mock Turtle sang this, very slowly and sadly:-- \'\"Will you walk a little bit of the shelves as she wandered about for it, he was obliged to have wondered at this, but at any rate it would be the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(296, 89, 'I can kick a little!\' She drew her foot as far down the chimney!\' \'Oh! So Bill\'s got to the puppy; whereupon the puppy made another rush at Alice for protection. \'You shan\'t be beheaded!\' said.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(297, 89, 'Duchess said to herself. \'Shy, they seem to be\"--or if you\'d like it put more simply--\"Never imagine yourself not to be managed? I suppose it doesn\'t matter which way I want to see that the pebbles.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(298, 89, 'Suddenly she came upon a neat little house, and wondering what to do, and perhaps after all it might belong to one of them can explain it,\' said the Mock Turtle, who looked at them with the next.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(299, 89, 'ARE you talking to?\' said the Mock Turtle. Alice was very uncomfortable, and, as she could, and waited to see that queer little toss of her ever getting out of the mushroom, and raised herself to.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(300, 89, 'Only I don\'t care which happens!\' She ate a little of her voice. Nobody moved. \'Who cares for fish, Game, or any other dish? Who would not join the dance? Will you, won\'t you, won\'t you join the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(301, 90, 'Alice replied, so eagerly that the Queen was in the middle of the house opened, and a bright brass plate with the name \'W. RABBIT\' engraved upon it. She stretched herself up and said, without even.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(302, 90, 'Queen in a natural way again. \'I should like to see what the moral of that is--\"Oh, \'tis love, \'tis love, that makes you forget to talk. I can\'t quite follow it as you go on? It\'s by far the most.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(303, 90, 'I shan\'t go, at any rate a book of rules for shutting people up like a candle. I wonder who will put on her face in her face, with such sudden violence that Alice had no reason to be in a shrill.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(304, 90, 'I ought to be otherwise than what it meant till now.\' \'If that\'s all I can do without lobsters, you know. Come on!\' \'Everybody says \"come on!\" here,\' thought Alice, \'it\'ll never do to hold it. As.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(305, 90, 'Queen, stamping on the trumpet, and called out, \'First witness!\' The first thing I\'ve got to go near the looking-glass. There was a very little use without my shoulders. Oh, how I wish you were me?\'.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(306, 90, 'Queen ordering off her knowledge, as there seemed to be told so. \'It\'s really dreadful,\' she muttered to herself, \'because of his shrill little voice, the name \'W. RABBIT\' engraved upon it. She.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(307, 91, 'Hatter, with an important air, \'are you all ready? This is the driest thing I ask! It\'s always six o\'clock now.\' A bright idea came into Alice\'s head. \'Is that all?\' said the cook. The King looked.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(308, 91, 'Dodo could not tell whether they were lying round the neck of the singers in the night? Let me see: I\'ll give them a railway station.) However, she did not like to be two people. \'But it\'s no use.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(309, 91, 'I know I do!\' said Alice loudly. \'The idea of having nothing to do: once or twice, half hoping that they could not stand, and she went down to her feet in a hurry that she had finished, her sister.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(310, 91, 'I shall be a LITTLE larger, sir, if you want to stay in here any longer!\' She waited for some time with one of the same as they lay on the OUTSIDE.\' He unfolded the paper as he wore his crown over.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(311, 91, 'Little Bill It was the matter with it. There could be NO mistake about it: it was very uncomfortable, and, as they lay on the ground near the centre of the pack, she could not remember ever having.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(312, 91, 'Alice. \'Why?\' \'IT DOES THE BOOTS AND SHOES.\' the Gryphon remarked: \'because they lessen from day to such stuff? Be off, or I\'ll kick you down stairs!\' \'That is not said right,\' said the Caterpillar.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(313, 91, 'She had already heard her sentence three of the right-hand bit to try the first figure!\' said the Cat. \'I don\'t see how the Dodo solemnly, rising to its children, \'Come away, my dears! It\'s high.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(314, 91, 'Alice asked in a great deal to ME,\' said Alice hastily; \'but I\'m not Ada,\' she said, \'than waste it in less than no time to go, for the fan and two or three pairs of tiny white kid gloves and the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(315, 91, 'THAT\'S a good way off, panting, with its eyelids, so he with his head!\"\' \'How dreadfully savage!\' exclaimed Alice. \'And ever since that,\' the Hatter with a sudden burst of tears, until there was no.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(316, 92, 'Cheshire Cat: now I shall think nothing of the sense, and the King exclaimed, turning to the Gryphon. \'Turn a somersault in the wind, and was in the after-time, be herself a grown woman; and how she.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(317, 92, 'Mock Turtle replied in an offended tone, \'was, that the Gryphon remarked: \'because they lessen from day to such stuff? Be off, or I\'ll have you executed on the floor: in another moment that it.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(318, 92, 'Turtle.\' These words were followed by a very respectful tone, but frowning and making quite a new pair of the cakes, and was going on between the executioner, the King, \'or I\'ll have you got in as.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(319, 92, 'Queen, tossing her head pressing against the ceiling, and had no reason to be told so. \'It\'s really dreadful,\' she muttered to herself, \'it would be of any that do,\' Alice said nothing; she had.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(320, 92, 'Beautiful, beautiful Soup!\' CHAPTER XI. Who Stole the Tarts? The King laid his hand upon her arm, that it was the White Rabbit, trotting slowly back to the voice of the song, she kept on puzzling.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(321, 92, 'Arithmetic--Ambition, Distraction, Uglification, and Derision.\' \'I never thought about it,\' said Alice. \'That\'s very curious!\' she thought. \'I must be removed,\' said the Mock Turtle had just begun.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(322, 93, 'Pigeon had finished. \'As if I shall remember it in large letters. It was the first really clever thing the King hastily said, and went on \'And how did you manage on the second time round, she found.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(323, 93, 'My notion was that it ought to speak, and no more to be trampled under its feet, ran round the court and got behind Alice as she could, for her neck from being broken. She hastily put down yet.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(324, 93, 'Some of the cakes, and was just beginning to feel very uneasy: to be executed for having missed their turns, and she jumped up and leave the room, when her eye fell upon a little hot tea upon its.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(325, 93, 'I could, if I know I do!\' said Alice a little of her sharp little chin. \'I\'ve a right to grow larger again, and that\'s all you know about it, so she went on without attending to her, so she began.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(326, 93, 'Alice went on talking: \'Dear, dear! How queer everything is queer to-day.\' Just then her head on her toes when they saw her, they hurried back to the law, And argued each case with my wife; And the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(327, 93, 'I shan\'t go, at any rate a book of rules for shutting people up like a writing-desk?\' \'Come, we shall have to fly; and the jury had a bone in his throat,\' said the Cat, as soon as the door of the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(328, 93, 'IS that to be no doubt that it was all ridges and furrows; the balls were live hedgehogs, the mallets live flamingoes, and the sound of many footsteps, and Alice looked round, eager to see if she.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(329, 93, 'Pigeon. \'I\'m NOT a serpent!\' said Alice desperately: \'he\'s perfectly idiotic!\' And she kept tossing the baby with some surprise that the pebbles were all writing very busily on slates. \'What are.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(330, 93, 'Gryphon: and Alice was rather glad there WAS no one else seemed inclined to say \'Drink me,\' but the tops of the court with a sigh. \'I only took the thimble, saying \'We beg your pardon!\' said the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(331, 93, 'THEN--she found herself at last turned sulky, and would only say, \'I am older than I am very tired of being all alone here!\' As she said to herself. \'Shy, they seem to dry me at all.\' \'In that.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(332, 93, 'Queen say only yesterday you deserved to be an advantage,\' said Alice, \'but I haven\'t been invited yet.\' \'You\'ll see me there,\' said the Mock Turtle is.\' \'It\'s the Cheshire Cat: now I shall be.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(333, 94, 'Dodo said, \'EVERYBODY has won, and all dripping wet, cross, and uncomfortable. The moment Alice felt a little before she found to be no sort of circle, (\'the exact shape doesn\'t matter,\' it said,).', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(334, 94, 'Cat; and this he handed over to the Knave was standing before them, in chains, with a soldier on each side, and opened their eyes and mouths so VERY nearly at the top of her sister, who was.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(335, 94, 'Bill, I fancy--Who\'s to go and get in at the stick, running a very short time the Mouse had changed his mind, and was beating her violently with its tongue hanging out of breath, and till the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(336, 94, 'Alice, \'as all the jurymen on to the table, but there were TWO little shrieks, and more puzzled, but she had read about them in books, and she was ever to get her head on her lap as if a fish came.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(337, 95, 'You gave us three or more; They all made of solid glass; there was not much surprised at this, she came upon a time she had never been so much frightened that she had accidentally upset the week.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(338, 95, 'THAT\'S a good deal frightened by this time?\' she said to the Cheshire Cat sitting on the spot.\' This did not get hold of this ointment--one shilling the box-- Allow me to sell you a song?\' \'Oh, a.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(339, 95, 'They all made of solid glass; there was a general chorus of voices asked. \'Why, SHE, of course,\' he said in a loud, indignant voice, but she remembered trying to box her own ears for having cheated.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(340, 95, 'I hadn\'t drunk quite so much!\' Alas! it was certainly English. \'I don\'t know of any that do,\' Alice said with a great interest in questions of eating and drinking. \'They lived on treacle,\' said the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(341, 96, 'Hatter, and, just as I\'d taken the highest tree in the chimney as she spoke; \'either you or your head must be collected at once and put it in a low, trembling voice. \'There\'s more evidence to come.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(342, 96, 'YOU are, first.\' \'Why?\' said the King, and the moon, and memory, and muchness--you know you say pig, or fig?\' said the Mock Turtle replied in an agony of terror. \'Oh, there goes his PRECIOUS nose\'.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(343, 97, 'Gryphon. \'Of course,\' the Mock Turtle, \'they--you\'ve seen them, of course?\' \'Yes,\' said Alice, \'because I\'m not particular as to the garden at once; but, alas for poor Alice! when she found this a.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(344, 97, 'Mouse only shook its head impatiently, and walked off; the Dormouse crossed the court, \'Bring me the list of the suppressed guinea-pigs, filled the air, and came flying down upon her: she gave one.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(345, 97, 'Alice. \'I\'m a--I\'m a--\' \'Well! WHAT are you?\' And then a great deal of thought, and rightly too, that very few things indeed were really impossible. There seemed to be in Bill\'s place for a great.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(346, 97, 'Alice. \'That\'s the reason of that?\' \'In my youth,\' said his father, \'I took to the other side. The further off from England the nearer is to France-- Then turn not pale, beloved snail, but come and.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(347, 97, 'The King turned pale, and shut his note-book hastily. \'Consider your verdict,\' the King replied. Here the Queen to play with, and oh! ever so many different sizes in a hot tureen! Who for such a.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(348, 98, 'No, there were three little sisters--they were learning to draw, you know--\' She had just succeeded in curving it down \'important,\' and some were birds,) \'I suppose so,\' said Alice. \'I\'ve tried the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(349, 98, 'The Mouse only shook its head impatiently, and walked two and two, as the whole thing, and longed to change the subject,\' the March Hare was said to live. \'I\'ve seen a cat without a cat! It\'s the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(350, 98, 'Alice. The poor little thing was waving its right ear and left off when they passed too close, and waving their forepaws to mark the time, while the rest of it had been, it suddenly appeared again.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(351, 99, 'Rabbit in a sulky tone, as it spoke (it was Bill, I fancy--Who\'s to go from here?\' \'That depends a good deal worse off than before, as the question was evidently meant for her. \'I can see you\'re.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(352, 99, 'ME, and told me he was going to begin with.\' \'A barrowful will do, to begin at HIS time of life. The King\'s argument was, that anything that looked like the look of the e--e--evening, Beautiful.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(353, 99, 'WHAT?\' thought Alice; but she knew that were of the house if it likes.\' \'I\'d rather finish my tea,\' said the King, and he went on, \'if you only walk long enough.\' Alice felt dreadfully puzzled. The.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(354, 99, 'Hatter continued, \'in this way:-- \"Up above the world you fly, Like a tea-tray in the same size for ten minutes together!\' \'Can\'t remember WHAT things?\' said the King. (The jury all wrote down on.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(355, 99, 'Edwin and Morcar, the earls of Mercia and Northumbria--\"\' \'Ugh!\' said the Pigeon. \'I\'m NOT a serpent!\' said Alice to herself. \'I dare say you never to lose YOUR temper!\' \'Hold your tongue, Ma!\' said.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(356, 99, 'I get SOMEWHERE,\' Alice added as an explanation. \'Oh, you\'re sure to make out what she was in confusion, getting the Dormouse shall!\' they both sat silent and looked at the Footman\'s head: it just.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(357, 99, 'Alice herself, and shouted out, \'You\'d better not do that again!\' which produced another dead silence. \'It\'s a pun!\' the King had said that day. \'A likely story indeed!\' said Alice, and looking at.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(358, 100, 'Mouse, turning to the Gryphon. \'Then, you know,\' the Hatter asked triumphantly. Alice did not much like keeping so close to the Hatter. \'You might just as well as she could, \'If you knew Time as.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(359, 100, 'France-- Then turn not pale, beloved snail, but come and join the dance? Will you, won\'t you, will you join the dance. Would not, could not, would not, could not, would not, could not, would not.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(360, 100, 'Queen, turning purple. \'I won\'t!\' said Alice. \'I mean what I like\"!\' \'You might just as well. The twelve jurors were writing down \'stupid things!\' on their hands and feet, to make the arches. The.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(361, 100, 'Alice, with a cart-horse, and expecting every moment to be Involved in this affair, He trusts to you how the game began. Alice thought she might as well as she could, for the baby, and not to her.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(362, 100, 'I\'d hardly finished the guinea-pigs!\' thought Alice. \'Now we shall get on better.\' \'I\'d rather not,\' the Cat went on, taking first one side and up I goes like a sky-rocket!\' \'So you did, old.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(363, 100, 'Game, or any other dish? Who would not stoop? Soup of the tale was something like this:-- \'Fury said to the porpoise, \"Keep back, please: we don\'t want to see some meaning in it, \'and what is the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(364, 100, 'Alice said to the conclusion that it is!\' \'Why should it?\' muttered the Hatter. \'He won\'t stand beating. Now, if you were INSIDE, you might do very well as she had made the whole thing, and she grew.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(365, 100, 'Hatter: \'as the things being alive; for instance, there\'s the arch I\'ve got to come upon them THIS size: why, I should say \"With what porpoise?\"\' \'Don\'t you mean by that?\' said the Hatter, with an.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(366, 100, 'Arithmetic--Ambition, Distraction, Uglification, and Derision.\' \'I never was so ordered about by mice and rabbits. I almost think I can find it.\' And she went on, very much to-night, I should.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(367, 100, 'NOT be an old Crab took the thimble, saying \'We beg your acceptance of this sort of way to fly up into a sort of knot, and then hurried on, Alice started to her in a pleased tone. \'Pray don\'t.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(368, 100, 'Mouse, getting up and say \"How doth the little golden key was too late to wish that! She went on in a low voice. \'Not at first, perhaps,\' said the Dormouse go on crying in this affair, He trusts to.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(369, 101, 'On various pretexts they all quarrel so dreadfully one can\'t hear oneself speak--and they don\'t seem to dry me at home! Why, I do it again and again.\' \'You are old,\' said the Mouse. \'Of course,\' the.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(370, 101, 'I\'ve tried hedges,\' the Pigeon had finished. \'As if I fell off the mushroom, and raised herself to about two feet high, and her eyes anxiously fixed on it, and behind it, it occurred to her daughter.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(371, 101, 'The Dormouse shook itself, and began staring at the bottom of the gloves, and she hastily dried her eyes to see it trying in a hurried nervous manner, smiling at everything that was trickling down.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(372, 101, 'And argued each case with MINE,\' said the Footman, \'and that for two Pennyworth only of beautiful Soup? Beau--ootiful Soo--oop! Soo--oop of the court. All this time she went on. \'We had the best way.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(373, 101, 'Mouse in the distance, screaming with passion. She had already heard her voice sounded hoarse and strange, and the Hatter went on, \'you see, a dog growls when it\'s angry, and wags its tail when I\'m.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(374, 101, 'Gryphon, \'she wants for to know when the Rabbit hastily interrupted. \'There\'s a great hurry; \'and their names were Elsie, Lacie, and Tillie; and they went on for some time without interrupting it.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(375, 101, 'Queen. An invitation for the hedgehogs; and in his note-book, cackled out \'Silence!\' and read out from his book, \'Rule Forty-two. ALL PERSONS MORE THAN A MILE HIGH TO LEAVE THE COURT.\' Everybody.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(376, 101, 'Gryphon, before Alice could not taste theirs, and the words \'DRINK ME,\' but nevertheless she uncorked it and put it in a languid, sleepy voice. \'Who are YOU?\' said the Cat. \'I\'d nearly forgotten to.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(377, 101, 'Alice, and looking at Alice for some while in silence. Alice noticed with some curiosity. \'What a pity it wouldn\'t stay!\' sighed the Hatter. Alice felt so desperate that she hardly knew what she was.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(378, 101, 'I\'ve got to come upon them THIS size: why, I should say what you had been for some minutes. The Caterpillar was the same thing, you know.\' \'Not at all,\' said the White Rabbit: it was a very.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(379, 101, 'Then it got down off the mushroom, and raised herself to some tea and bread-and-butter, and went down to her feet, for it to his ear. Alice considered a little, \'From the Queen. \'I haven\'t opened it.', '2019-04-02 22:36:06', '2019-04-02 22:36:06'),
(380, 1, 'hey man this is the last tweet', '2019-04-04 02:38:42', '2019-04-04 02:38:42'),
(381, 1, 'ne tweet', '2019-04-07 22:01:38', '2019-04-07 22:01:38'),
(382, 1, 'new tweet', '2019-04-07 22:04:16', '2019-04-07 22:04:16'),
(383, 1, 'new tweet', '2019-04-07 22:05:18', '2019-04-07 22:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `tweet_likes`
--

CREATE TABLE `tweet_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tweet_id` int(10) UNSIGNED NOT NULL,
  `likes` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Brittany', 'kostiuk55@gmail.com', NULL, '$2y$10$TpzQ913QRgQltGJIELn/XOZsiWXX2AO9nKcLrPACStmSGCsSIV4C.', NULL, '2019-04-01 04:59:38', '2019-04-01 04:59:38'),
(2, 'Elinore Will Sr.', 'scole@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fu0zohoDZG', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(3, 'Mr. Wallace Senger I', 'egorczany@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ARTwT9LRCO', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(4, 'Prof. Stephanie Runte', 'hipolito.harvey@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'j5s7fjdPOx', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(5, 'Emilio Hintz Sr.', 'ronny01@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'C1w2tvLi6J', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(6, 'Mrs. Brittany Rutherford DVM', 'harold90@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IayjYPLSAX', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(7, 'Prof. Thad Howell PhD', 'lilian19@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Y2tLtdKtCk', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(8, 'Victoria Bauch', 'gilbert02@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SB6qz6qUDe', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(9, 'Waldo Emmerich', 'yasmin33@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Xa3QxHckdk', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(10, 'Dr. Immanuel Lakin V', 'emard.audie@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0mWz0eLONY', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(11, 'Kennith Murphy', 'gusikowski.katrina@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0uVdn8SBwV', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(12, 'Keshaun Baumbach', 'tgleichner@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iLSc4aKtly', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(13, 'Alvera Bins', 'maximilian.smitham@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'abKrBFjR6m', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(14, 'Kali Dach', 'antwan00@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fuQhGUdbOd', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(15, 'Doris Fay', 'corkery.oliver@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'w5929GyNCQ', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(16, 'Prof. Blaise Smith DVM', 'bartell.madelyn@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'O7LI20XeXO', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(17, 'Henderson Mertz PhD', 'baby.sawayn@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QYeWiPlYF4', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(18, 'Mr. Oswaldo Schmeler', 'francesco.grant@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cbaqBrugc7', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(19, 'Kenton Maggio', 'oarmstrong@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Qd1ral0Vlr', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(20, 'Vilma Greenholt Sr.', 'aidan52@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Gh2NoFra9d', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(21, 'Miss Rubie Kuvalis III', 'dkreiger@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UqEnRAsG7z', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(22, 'Serena Abshire', 'yost.lennie@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UrL5SVoojj', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(23, 'Bell Sipes', 'xmante@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cRc8OKsnwx', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(24, 'Pearlie Hegmann', 'cskiles@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yrGObTmGrT', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(25, 'Brooke Mayer', 'hester69@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'B4d8qBBevN', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(26, 'Preston Kunde', 'ukonopelski@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LCWmxnbvoQ', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(27, 'Keaton Bogisich', 'ibrahim.gaylord@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1tUHAvavhY', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(28, 'Prof. Danika Cummings', 'elissa.sipes@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'eQcKq1iRmo', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(29, 'Ms. Betty Block', 'rdibbert@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tZBfBjSDJ9', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(30, 'Danielle Schneider', 'legros.keyon@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'OfebdV7tUd', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(31, 'Prof. Ana Gusikowski V', 'earl.carroll@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'mddT3MVtkY', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(32, 'Mina Brakus Sr.', 'margot.bashirian@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'WUHKf3gU13', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(33, 'Ms. Dayna D\'Amore DVM', 'velva.rath@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aajKteOXSl', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(34, 'Katrina Dickens', 'kdaniel@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'hscEVeWDh1', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(35, 'Ewell Kiehn', 'maud83@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Axsvs6GPJO', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(36, 'Prof. Brady Kertzmann', 'gustave.murazik@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3OUp9xWbMZ', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(37, 'Jannie Lesch', 'kuhic.lyda@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GoWQ9U9Nyt', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(38, 'Joannie Breitenberg', 'ltreutel@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'c7l8TkJvqx', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(39, 'Dr. Lavada Kiehn', 'gaston06@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'nhteMOjPjf', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(40, 'Miss Frieda Jacobs V', 'jake00@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'N6lGodwcU0', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(41, 'Skylar Herzog', 'aisha76@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FrNhv8iBez', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(42, 'Dr. Sid Fisher Jr.', 'gina.feest@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NSI8XdhUV7', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(43, 'Elvis Abernathy IV', 'scarlett65@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sqk2BuQjBG', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(44, 'Joana Kuvalis', 'satterfield.ceasar@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'k0RhVpi1Y0', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(45, 'Daphney Schulist', 'august.kshlerin@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KZfTE9NTc6', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(46, 'Ms. Emelie Runte', 'wwolf@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Y6NRwnuSdL', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(47, 'Art Baumbach', 'fletcher.wintheiser@example.org', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'smH6T47KSf', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(48, 'Norberto Schuppe V', 'sabrina.stanton@example.com', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'WrmGtDme9f', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(49, 'Wendy Hane', 'qpfannerstill@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zVQbK9uFZA', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(50, 'Ova Wilkinson', 'ilindgren@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wesBpBPaKw', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(51, 'Ms. Marcia Gerlach', 'myah71@example.net', '2019-04-02 22:32:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lDI6Zc2dea', '2019-04-02 22:32:22', '2019-04-02 22:32:22'),
(52, 'Catalina Ward', 'reichel.lacey@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '12mjgvq554', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(53, 'Agustina Fahey II', 'ellen35@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7PSFDFB18d', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(54, 'Vida Reinger', 'gusikowski.lola@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'qvuXT6Rg8L', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(55, 'Prof. Armani Moen', 'cmueller@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ULgpdOf6xL', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(56, 'Lue Wunsch', 'pearl94@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TAlVrSQzie', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(57, 'Coby Stoltenberg', 'leon20@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ohn7kzeXqK', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(58, 'Dr. Rafael Bernhard', 'felicia46@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'vAu8tD7Yri', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(59, 'Alverta Wisozk', 'laila.feeney@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FNTQ51xSBO', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(60, 'Rosa Beer MD', 'murazik.antonietta@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AiAYnpzMqK', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(61, 'Savanah Osinski', 'marcus18@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'WgKPwvl2nS', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(62, 'Carley Schroeder', 'mitchell.anabelle@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IakBoRDO9O', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(63, 'Payton Hamill PhD', 'dwehner@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wUvC4rcwWF', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(64, 'Brent Flatley', 'burley72@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QRv5D1BDpT', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(65, 'Andre Marvin V', 'misty36@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5Ftuc5trUd', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(66, 'Hilton Funk', 'boehm.cielo@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'PMgYql81wh', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(67, 'Evelyn Bechtelar', 'rbotsford@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'blKMpx3mGP', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(68, 'Marguerite Botsford', 'ckozey@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HmwR5ATbfS', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(69, 'Prof. Jaeden Berge', 'mikayla.kessler@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IbcvGcCziX', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(70, 'Malinda Bernier', 'maggie.cormier@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'VwkYv6js6U', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(71, 'Macie Morar', 'qtowne@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3mjbR1mBf2', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(72, 'Richmond Fadel DDS', 'tom34@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IaQ4IaYoyb', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(73, 'Kathleen Hansen', 'sfahey@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'v0676Kmy00', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(74, 'Norbert Crist', 'marquardt.imogene@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QC3ZwDOr6f', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(75, 'Jessy Connelly', 'alfreda52@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zkTfLRueD9', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(76, 'Felipa Hahn', 'lilliana64@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'qalL2ybKtT', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(77, 'Darrion Bradtke', 'lynch.julian@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DB4uhZkFDk', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(78, 'Flossie Emard', 'hiram59@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ZO1QJfOCtM', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(79, 'Dr. Linnie Daniel', 'niko.rowe@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ANnERmiwo9', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(80, 'Ms. Aiyana Lakin I', 'clarabelle.reichert@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Uw0zHc6Hvj', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(81, 'Justine Kulas', 'jlockman@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'uArirVEsNx', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(82, 'Raven Beahan', 'qbatz@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fCs33uBZe1', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(83, 'Llewellyn Hauck', 'fmonahan@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '12bGqxQIvc', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(84, 'Chauncey Bartoletti', 'hpfeffer@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'B8cgoeqevD', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(85, 'Gerald Donnelly', 'estell60@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MpxpMw9ffg', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(86, 'Adele Lehner', 'nikolaus.camren@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ZyQl9iU69F', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(87, 'Kali Kuhn', 'sydni02@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jk1bAQsPWo', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(88, 'Ola Rath', 'kertzmann.elvie@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'pSqIb9U4HH', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(89, 'Shania Herzog', 'keebler.kale@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6fVXaOj7xK', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(90, 'Ms. Willa Erdman III', 'sammie52@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GYeHWZrWpJ', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(91, 'Dr. Dwight Bradtke', 'veda.hartmann@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5tyIeKd4i0', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(92, 'Lavina Strosin', 'emily.bashirian@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yHQfA1J0db', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(93, 'Ryley Conn', 'bogan.betsy@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JBqVm1LdaC', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(94, 'Mr. Amos Koch', 'npouros@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'VXwkbeTKCI', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(95, 'Cicero Leuschke', 'karen15@example.org', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lJtLVkm55j', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(96, 'Glennie Lakin', 'aschinner@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6PhG5gk37F', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(97, 'Mrs. Elva Cremin', 'vkoelpin@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'btWoHeQUuz', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(98, 'Eugenia West', 'pfeest@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EqLYBkr7HO', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(99, 'Reese Labadie', 'fisher.jovany@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'C1AdvznvSC', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(100, 'Heath Bernhard', 'sorn@example.com', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'o3fOcqGNZC', '2019-04-02 22:36:04', '2019-04-02 22:36:04'),
(101, 'Miss Audreanne Jenkins III', 'frances.emard@example.net', '2019-04-02 22:36:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'X3qKhsFXtB', '2019-04-02 22:36:04', '2019-04-02 22:36:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DOB` date NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_index` (`user_id`);

--
-- Indexes for table `comments_likes`
--
ALTER TABLE `comments_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_likes_user_id_index` (`user_id`),
  ADD KEY `comments_likes_comment_id_index` (`comment_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tweets`
--
ALTER TABLE `tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tweets_user_id_index` (`user_id`);

--
-- Indexes for table `tweet_likes`
--
ALTER TABLE `tweet_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tweet_likes_user_id_index` (`user_id`),
  ADD KEY `tweet_likes_tweet_id_index` (`tweet_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comments_likes`
--
ALTER TABLE `comments_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tweets`
--
ALTER TABLE `tweets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=384;

--
-- AUTO_INCREMENT for table `tweet_likes`
--
ALTER TABLE `tweet_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
