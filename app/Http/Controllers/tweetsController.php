<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweets;
use App\Comments;
use App\User_detail;
use App\User;
use App\TweetLikes;
use App\Following;
use Auth;

class tweetsController extends Controller
{
    public function createTweets(Request $request){
            // post createpost
            $user=Auth::User();
            $tweets =new Tweets;
            $tweets->tweets=$request->tweet;
            $tweets->user_id=$user->id;
            $tweets->save();

            $tweets=Tweets::orderBy('created_at', 'DESC')->get();
            return view('dashboard', compact('tweets'));
        }

    public function createComments(Request $request){
        $user=Auth::User();
        $comments =new Comments;
        $comments->tweets=$request->tweet;
        $comments->tweet_id=$user->id;
        $comments->save();

        $comments=Comments::orderBy('created_at', 'DESC')->get();
        return view('dashboard', compact('comments'));
    }
}
