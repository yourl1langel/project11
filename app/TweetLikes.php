<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TweetLikes extends Model
{
    public function tweets(){
        return $this->belongsTo('App\Tweets');
    }
}
