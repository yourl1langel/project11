<?php

use Illuminate\Database\Seeder;
use Illuminate\support\str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        factory(App\User::class, 50)->create()->each(function ($user) {
          for ($i=0; $i <=rand(1,30) ; $i++) {
              $user->tweets()->save(factory(App\Tweets::class)->make());
          }
      });

    }
}
