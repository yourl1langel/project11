@extends('partials.nav')
@extends('partials.profilecard')
    <link rel="stylesheet" href="{{ URL::asset('css/pages.css') }} ">
        <div class="comment-body">
{{-- postform for user to tweet --}}

            <div class="form-group posts-displayform">
                <form action="createpost" method="post">
                    @csrf
                    <textarea class="form-control posts-textarea" rows="1" name="tweet"
                    placeholder="what's happening" required></textarea>
                    <button class="tweet-button"type="submit" name="tweet-button">Tweet</button>
                </form>
                <footer>gif block here</footer>
            </div>

            <section class="userPosts">
                <div class="col-md-6 col-md-offset-3 postContainer">
                    <article class="postContent">
                        <div id='tweetsWrapper'><tweet-component v-for='tweet in tweets' :tweet=tweet></tweet-component></div>
                    </article>
                </div>
            </section>
            <script> currentLoggedInUser = {{ $user->id }}</script>
            <script src="{{ asset('js/app.js') }}" defer></script>
    </body>
</html>
