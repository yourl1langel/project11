<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="a new social media to connect to new and old friends all over the world of locally">
        <meta name="keywords" content="connect, friends, sports, poltics, memes">
        <meta name="robots" content="index, follow">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ URL::asset('css/marketing.css') }} ">
        <title>Tweeter</title>
    </head>
    <body>

    <a href="index"><i class="fas fa-home fa-2x"></i></a>


    <section class="slide height--100" id="intro">
        <div class="slide__bg-img background"><video src="{{URL::asset('videos/smoke.mp4')}}" autoplay muted></video></div>
        <div class="slide__inner-wrapper">
            <div class="slide__inner-wrapper slide__inner-wrapper--left">
                <p class="slide__description slideUp slideUp--delay-2">WELCOME</p>
                <h1 class="slide__description slideUp slideUp--delay-2">TO</h1>
                    <section class="smokeContainer">
                        <h2 class="slide__heading slideUp setBack">
                            <span class="smoke">T</span>
                            <span class="smoke">W</span>
                            <span class="smoke">E</span>
                            <span class="smoke">E</span>
                            <span class="smoke">T</span>
                            <span class="smoke">E</span>
                            <span class="smoke">R</span>
                        </h2>
                    </section>
                    <p id='setDown' class="slide__description slideUp slideUp--delay-3 animated infinite heartbeat delay-2s">where YOUR interests are important!</p>
            </div>
                 </div>
             </div>
         </section>
         <section class="content">
             <div class="content__wrapper">
                 <h2 class="content__heading slideUp">Keep up to date</h2>
                 <p class="content__description slideUp slideUp--delay-2">with all the new trends, sports or politics</p>
             </div>
        </section>
        <section class="slide height--100" id="">
            <div class="slide__bg-img" ><img style=" background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAAwqaAELG491qdVkLagcPmQkfpl4fuE0q58KIuQfZA6qIzUkcPQ')" alt="connections"></div>
            <div class="slide__inner-wrapper slide__inner-wrapper--top">
                <h2 class="slide__heading slideUp">CONNECT</h2>
                <p class="slide__description slideUp slideUp--delay-2">Make new friends<br>anywhere around the world or stay local!</p>
            </div>
        </section>
        <section class="content">
            <div class="content__wrapper">
                <h2 class="content__heading slideUp">focus</h2>
                <p class="content__description slideUp slideUp--delay-2">Focus on the most IMPORTANT thing. What YOUR interested in!</p>
            </div>
        </section>
        <section class="slide height--100" id="">
            <div class="slide__bg-img"><img style="background-image: url('http://www.teamopenoffice.org/pimages/23/235123/mysterious-wallpaper.jpg')" alt=""> </div>
            <div class="slide__inner-wrapper slide__inner-wrapper--left">
                <h2 class="slide__heading slideUp">UPLOAD YOUR BEST PHOTOS</h2>
                <p class="slide__description slideUp slideUp--delay-2">A picture is worth a thousand words</p>
                <p class="slide__description slideUp slideUp--delay-3">Let the images do the talking and you can<br>use text for short summaries.</p>
            </div>
        </section>
        <section class="content">
            <div class="content__wrapper">
                <h2 class="content__heading slideUp">HAVE A BUISNESS?</h2>
                <p class="content__description slideUp slideUp--delay-2">Advertise it in our market!</p>
            </div>
        </section>
        <section class="slide height--100">
            <div class="slide__bg-img"><canvas id="c"></canvas></div>
            <div class="slide__inner-wrapper slide__inner-wrapper--top slide__inner-wrapper--black">
                <h2 class="slide__heading slideUp">Want to create something amazing?</h2>
                <p class="slide__description slideUp slideUp--delay-2">It starts with one big step.</p>
                <span class="slide__download-btn slideUp slideUp--delay-3"><a href='register'>Sign Up Now</a></span>
            </div>
        </section>




        {{-- colorsplash: --}}
        <script src='https://cdnjs.cloudflare.com/ajax/libs/animejs/2.2.0/anime.js'></script>

        <script src="{{ asset('js/marketing.js') }}" defer></script>
        {{-- scrollmagic: --}}
        <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js"></script>
        {{-- bootstrap --}}
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
