<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Tweeter</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ URL::asset('css/partials.css') }} ">
    </head>
    <body class="color">
        <header>
            <div class="nav-container">
                <nav>
                    <ul>
                        <li><a href="/dashboard">Home</a></li>
                        <li><a href="/posts">Moments</a></li>
                        <li><a href="/notification">Notifications</a></li>
                        <li><a href="/messages">Messages</a></li>
                    </ul>
                </nav>
                <img class="icon" src="{{ URL::asset('images/tweetericon.png') }}"
                alt="tweeterimg">
                <div class="right-nav">
                    <input type="text" name="search">

                    <button type="submit" name="tweet">Tweet</button>
                </div>
            </div>
{{-- for logout --}}
            {{-- @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                             {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li> --}}
</header>
    {{-- @endguest --}}
