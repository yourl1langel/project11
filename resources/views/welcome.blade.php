<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ URL::asset('css/tweeter.css') }} ">

        <title>welcome</title>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="background">
            <img src="{{ URL::asset('images/index-background.png') }}" alt="background">
            <div class="leftside">
                <div class="left">
                    <h1 class="search"><img src="{{ URL::asset('images/search.png') }}" alt="search">follow your interests.</h1>
                    <h1 class="people"><img src="{{ URL::asset('images/people.png') }}" alt="people">hear what people are talking about.</h1>
                    <h1 class`conv><img src="{{ URL::asset('images/conv.png') }}" alt="conversation">join the conversation.</h1>
                </div>
            </div>
            <div class="rightside">
                <div class="right">
                    <div class="login-home">
                        <img class="index-icon" src="{{ URL::asset('images/tweetericon.png') }}"
                        alt="tweeterimg">
                        <button class="tweeter-b0"type="submit" name="login">
                            <a href="/login">Log in</a>
                        </button>
                    </div>
                    <h2>See what's happening in the world right now</h2>
                    <h3>Join tweetter today.</h3>
                    <button class="tweeter-b1" type="submit" name="Signup">
                        <a href="/register">Sign Up</a>
                    </button>
                    <button class="tweeter-b2" type="submit" name="Login">
                        <a href="/login">Log in</a>
                    </button>
                </div>
            </div>
        </div>
        <footer>
            <ul>
                <li><a href="marketing">About</a></li>
                <li>Help Center</li>
                <li>Blog</li>
                <li>Status </li>
                <li>Jobs </li>
                <li>Terms </li>
                <li>Privacy Policy </li>
                <li>Cookies </li>
                <li>Ads info </li>
                <li>Brand </li>
                <li>Apps </li>
                <li>Advertise </li>
                <li>Marketing </li>
                <li>Businesses </li>
                <li>Developers </li>
                <li>Directory </li>
                <li>Settings </li>
                <li> &copy<?php echo date("Y"); ?> Tweetter </li>
            </ul>

        </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>
