<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/users', 'UserController@getAllUsers');

Route::get('/tweets', 'UserController@getAllTweets');
Route::get('/comments', 'UserController@getAllComments');

Route::get('/tweetlikes', 'UserController@getAllTweetsLikes');
Route::get('/commentlikes', 'UserController@getAllCommentsLikes');

Route::get('/gettweetsbynumber/{number}', 'UserController@getTweetsByNumber');
Route::get('/getcommentsbynumber/{number}', 'UserController@getCommentsByNumber');

Route::get('/tweetsbynumberfromstart/{number}/{id}', 'UserController@tweetsByNumberFromStart');

Route::post('/like-tweet', 'UserController@likeTweetViaApi');

Route::get('/tweet-comments/{tweetId}', 'UserController@getTweetCommentsByNumber');
Route::get('/new-comment', 'UserController@newTweetCommentsByApi');


// Route::get('/followers', 'UserController@getAllFollowers');
// Route::get('/following', 'UserController@getAllFollowing');
